module DttItemDetailx
  class ItemDetail < ActiveRecord::Base
    belongs_to :order_item, :class_name => DttItemDetailx.order_item_class.to_s
    
    #validates :order_item_id, :presence => true #validate causes error in @order_item.save order controller
    validates :order_item_id, :numericality => {:greater_than => 0}, :if => 'order_item_id.present?'
    #for workflow input validation  
    validate :dynamic_validate
    
    def validate_wf_input_data
      wf = Authentify::AuthentifyUtility.find_config_const('validate_item_detail_' + self.wf_event, 'dtt_item_detailx') if self.wf_event.present?
      if Authentify::AuthentifyUtility.find_config_const('wf_validate_in_config') == 'true' && wf.present? 
        eval(wf) 
      end
    end
    
    def dynamic_validate
      wf = Authentify::AuthentifyUtility.find_config_const('dynamic_validate_cob_info', 'dtt_item_detailx')
      eval(wf) if wf.present?
    end
  end
end
