require "dtt_item_detailx/engine"

module DttItemDetailx
  mattr_accessor :order_item_class
  
  def self.order_item_class
    @@order_item_class.constantize
  end
end
