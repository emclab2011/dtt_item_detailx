class CreateDttItemDetailxItemDetails < ActiveRecord::Migration
  def change
    create_table :dtt_item_detailx_item_details do |t|
      t.integer :order_item_id
      t.string :main_controller
      t.string :flash
      t.string :casing
      t.string :pcb_board
      t.text :material_note
      t.string :ratio
      t.string :speed
      t.string :embed_doc
      t.text :item_requirement_note
      t.string :laser
      t.string :logo_printing
      t.string :logo_sample_image
      t.string :item_accessory
      t.string :accessory_sample_image
      t.string :inner_box_size
      t.string :packing_accessory
      t.string :carton_size
      t.string :carton_content_label

      t.timestamps null: false
    end
    
    add_index :dtt_item_detailx_item_details, :order_item_id
  end
end
