require 'rails_helper'

module DttItemDetailx
  RSpec.describe ItemDetail, type: :model do

    it "should reject 0 order_item_id" do
      c = FactoryGirl.build(:dtt_item_detailx_item_detail, :order_item_id => 0)
      expect(c).not_to be_valid
    end
  end
end
