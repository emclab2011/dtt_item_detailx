FactoryGirl.define do
  factory :dtt_item_detailx_item_detail, :class => 'DttItemDetailx::ItemDetail' do
    order_item_id 1
    main_controller "MyString"
    flash "MyString"
    casing "MyString"
    pcb_board "MyString"
    material_note "MyText"
    ratio "MyString"
    speed "MyString"
    embed_doc "MyString"
    item_requirement_note "MyText"
    laser "MyString"
    logo_printing "MyString"
    logo_sample_image "MyString"
    item_accessory "MyString"
    accessory_sample_image "MyString"
    inner_box_size "MyString"
    packing_accessory "MyString"
    carton_size "MyString"
    carton_content_label "MyString"
  end

end
