Rails.application.routes.draw do

  mount DttItemDetailx::Engine => "/dtt_item_detailx"
  mount Authentify::Engine => '/auth'
  mount Commonx::Engine => '/common'
  mount MultiItemOrderx::Engine => '/order'
  
end
